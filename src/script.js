const grid_dimensions = 32;

const direction = { 
    up:     {x:0, y:1},
    down:   {x:0, y:-1},
    left:   {x:-1, y:0},
    right:  {x:1, y:0},
};

class character {
  constructor(){
      this.element = document.getElementById('default-bob');
      this.position = {
      x: 1, y: -10  
      };
      this.update_position();
      console.log(this.position);
  };
    move(dir) {
      dir = {
      x: direction.right.x , y: direction.up.y
      };
      this.position.x += dir.x;  
      this.position.y += dir.y;
      this.update_position();
      console.log(dir)
    };
    update_position() {
      this.element.style.top = (this.position.y * grid_dimensions) + "px";
      this.element.style.left = (this.position.x * grid_dimensions) + "px";
    };
  };


let bob = new character();
const character_name = document.getElementById('default-bob');

document.addEventListener('keydown', logKey);

function logKey(e) {
  let key = e.code;
  switch(key) {
      case 'ArrowUp':
          bob.move();
          break;
      case 'ArrowDown':
        bob.move();
          break;
      case 'ArrowLeft':
        bob.move();
          break;
      case 'ArrowRight':
        bob.move();
          break;
  };
};